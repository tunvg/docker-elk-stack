## Unifi Controller version 8.0.24

#### Override "Inform Host" IP

For your Unifi devices to "find" the Unifi Controller running in Docker, you _MUST_ override the Inform Host IP with the address of the Docker host computer (by default, the Docker container usually gets the internal address 172.17.x.x while Unifi devices connect to the (external) address of the Docker host). To do this:

* Find **Settings -> System -> Other Configuration -> Override Inform Host:** in the Unifi Controller web GUI (it's near the bottom of that page).
* Check the "Enable" box, and enter the IP address of the Docker host machine. 
* Save settings in Unifi Controller.
* Restart UniFi-in-Docker container with `docker stop ...` and `docker run ...` commands.

#### Volumes

Unifi looks for the `/unifi` directory (within the container) for its special purpose subdirectories:

* `/unifi/data` This contains your UniFi configuration data (formerly: `/var/lib/unifi`) .

* `/unifi/log` This contains UniFi log files (formerly: `/var/log/unifi`)

* `/unifi/cert` Place custom SSL certs in this directory. 
For more information regarding the naming of the certificates, see [Certificate Support](#certificate-support) (formerly: `/var/cert/unifi`).

* `/unifi/init.d`
You can place scripts you want to launch every time the container starts in here.

* `/var/run/unifi` 
Run information, in general you will not need to touch this volume.
It is there to ensure UniFi has a place to write its PID files.

#### Legacy Volumes

These are no longer actually volumes, rather they exist for legacy compatibility. You are urged to move to the new volumes ASAP.

* `/var/lib/unifi` New name: `/unifi/data`
* `/var/log/unifi` New name: `/unifi/log`

#### Environment Variables:

You can pass in environment variables using the `-e` option when you invoke `docker run...` See the `TZ` in the example above. Other environment variables:

* `UNIFI_HTTP_PORT`
This is the HTTP port used by the Web interface. Browsers will be redirected to the `UNIFI_HTTPS_PORT`.
**Default: 8080**

* `UNIFI_HTTPS_PORT`
This is the HTTPS port used by the Web interface.
**Default: 8443** 

* `PORTAL_HTTP_PORT`
Port used for HTTP portal redirection.
**Default: 80** 

* `PORTAL_HTTPS_PORT`
Port used for HTTPS portal redirection.
**Default: 8843** 

* `UNIFI_STDOUT`
Controller outputs logs to stdout in addition to server.log.
**Default: unset** 

* `TZ`
TimeZone (i.e Asia/Ho_Chi_Minh).

* `JVM_MAX_THREAD_STACK_SIZE`
Used to set max thread stack size for the JVM
Example:

  ```
  --env JVM_MAX_THREAD_STACK_SIZE=1280k
  ```

  as a fix for [https://community.ubnt.com/t5/UniFi-Routing-Switching/IMPORTANT-Debian-Ubuntu-users-MUST-READ-Updated-06-21/m-p/1968251#M48264](https://community.ubnt.com/t5/UniFi-Routing-Switching/IMPORTANT-Debian-Ubuntu-users-MUST-READ-Updated-06-21/m-p/1968251#M48264)

* `LOTSOFDEVICES`
Enable this with `true` if you run a system with a lot of devices and/or with a low powered system (like a Raspberry Pi). This makes a few adjustments to try and improve performance: 

  * enable unifi.G1GC.enabled
  * set unifi.xms to JVM\_INIT\_HEAP\_SIZE
  * set unifi.xmx to JVM\_MAX\_HEAP\_SIZE
  * enable unifi.db.nojournal
  * set unifi.dg.extraargs to --quiet

  See [the Unifi support site](https://help.ui.com/hc/en-us/articles/115005159588-UniFi-How-to-Tune-the-Network-Application-for-High-Number-of-UniFi-Devices) for an explanation of some of those options.
**Default: unset** 

* `JVM_EXTRA_OPTS`
Used to start the JVM with additional arguments.
**Default: unset** 

* `JVM_INIT_HEAP_SIZE`
Set the starting size of the javascript engine for example: `16384M`
**Default: unset** 

* `JVM_MAX_HEAP_SIZE`
Java Virtual Machine (JVM) allocates available memory. For larger installations a larger value is recommended. For memory constrained system this value can be lowered. 
**Default: 16384M** 

#### Exposed Ports

The Unifi-in-Docker container exposes the following ports. A minimal Unifi Controller installation requires you expose the first three with the `-p ...` option.

* 8080/tcp - Device command/control 
* 8443/tcp - Web interface + API 
* 3478/udp - STUN service 
* 8843/tcp - HTTPS portal _(optional)_
* 8880/tcp - HTTP portal _(optional)_
* 6789/tcp - Speed Test (unifi5 only) _(optional)_

See [UniFi - Ports Used](https://help.ubnt.com/hc/en-us/articles/218506997-UniFi-Ports-Used) for more information.

#### Run as non-root User

The default container runs Unifi Controller as root. The recommended `docker run...` command above starts Unifi Controller so the image runs as `unifi` (non-root) user with the uid/gid 999/999. You can also set your data and logs directories to be owned by the proper gid.

_Note:_ When you run as a non-root user, you will not be able to bind to lower ports by default (this would not necessary if you are using the default ports). If you must do this, also pass the `--sysctl net.ipv4.ip_unprivileged_port_start=0` option on the `docker run...` to bind to whatever port you wish.

#### Certificate Support

To use custom SSL certs, you must map a volume with the certs to `/unifi/cert`

They should be named:

```shell
cert.pem  # The Certificate
privkey.pem # Private key for the cert
chain.pem # full cert chain
```

If your certificate or private key have different names, you can set the environment variables `CERTNAME` and `CERT_PRIVATE_NAME` to the name of your certificate/private key, e.g. `CERTNAME=my-cert.pem` and `CERT_PRIVATE_NAME=my-privkey.pem`.

For letsencrypt certs, we'll autodetect that and add the needed Identrust X3 CA Cert automatically. In case your letsencrypt cert is already the chained certificate, you can set the `CERT_IS_CHAIN` environment variable to `true`, e.g. `CERT_IS_CHAIN=true`. This option also works together with a custom `CERTNAME`.

#### Certificates Using Elliptic Curve Algorithms

If your certs use elliptic curve algorithms, which currently seems to be the default with letsencrypt certs, you might additionally have to set the `UNIFI_ECC_CERT` environment variable to `true`, otherwise clients will fail to establish a secure connection. For example an attempt with `curl` will show:

```shell
% curl -vvv https://my.server.com:8443
curl: (35) error:1404B410:SSL routines:ST_CONNECT:sslv3 alert handshake failure
```

You can check your certificate for this with the following command:

```shell
% openssl x509 -text < cert.pem | grep 'Public Key Algorithm'
        Public Key Algorithm: id-ecPublicKey
```

If the output contains `id-ec` as shown in the example, then your certificate might be affected.
